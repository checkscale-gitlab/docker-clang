# Copyright 2019 Tymoteusz Blazejczyk
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

ARG ALPINE_VERSION=latest
FROM alpine:${ALPINE_VERSION} as alpine

FROM alpine as builder

ARG CLANG_VERSION=8.0.1

# Download, configure, build and install the Clang C/C++ compiler
RUN apk add --quiet --no-cache \
        linux-headers \
        ncurses-dev \
        libedit-dev \
        python3-dev \
        libffi-dev \
        zlib-dev \
        musl-dev \
        binutils \
        python3 \
        ninja \
        cmake \
        swig \
        gcc \
        g++ \
        git && \
    git clone \
        --branch llvmorg-${CLANG_VERSION} \
        --single-branch \
        --depth 1 \
        https://github.com/llvm/llvm-project.git && \
    find /llvm-project/libcxx -type f | \
        xargs sed --in-place 's/\(strtou\?ll\)_l(\(.\+\),\s*\S\+)/\1(\2)/' && \
    find /llvm-project/llvm/lib/ExecutionEngine/ -type f | \
        xargs sed --in-place 's/\(HAVE_EHTABLE_SUPPORT\s\+\)1/\10/' && \
    cmake \
        -GNinja \
        -DCMAKE_C_COMPILER=gcc \
        -DCMAKE_CXX_COMPILER=g++ \
        -DCMAKE_BUILD_TYPE=Release \
        -DCMAKE_INSTALL_PREFIX=/stage-1/install \
        -DLLVM_BUILD_DOCS=NO \
        -DLLVM_BUILD_TESTS=NO \
        -DLLVM_BUILD_EXAMPLES=NO \
        -DLLVM_BUILD_BENCHMARKS=NO \
        -DLLVM_INCLUDE_DOCS=NO \
        -DLLVM_INCLUDE_TESTS=NO \
        -DLLVM_INCLUDE_EXAMPLES=NO \
        -DLLVM_INCLUDE_BENCHMARKS=NO \
        -DLLVM_ENABLE_PIC=YES \
        -DLLVM_ENABLE_FFI=YES \
        -DLLVM_ENABLE_ZLIB=YES \
        -DLLVM_ENABLE_RTTI=YES \
        -DLLVM_ENABLE_SPHINX=NO \
        -DLLVM_ENABLE_DOXYGEN=NO \
        -DLLVM_ENABLE_OCAMLDOC=NO \
        -DLLVM_LINK_LLVM_DYLIB=YES \
        -DLLVM_BUILD_LLVM_DYLIB=YES \
        -DLLVM_INSTALL_BINUTILS_SYMLINKS=YES \
        -DLLVM_HOST_TRIPLE=$(uname -m)-alpine-linux-musl \
        -DLLVM_DEFAULT_TARGET_TRIPLE=$(uname -m)-alpine-linux-musl \
        -S /llvm-project/llvm \
        -B /stage-1/build/llvm && \
    cmake \
        --build /stage-1/build/llvm \
        --target all && \
    cmake \
        --build /stage-1/build/llvm \
        --target install && \
    export PATH=/stage-1/install/bin:$PATH && \
    cmake \
        -GNinja \
        -DCMAKE_C_COMPILER=gcc \
        -DCMAKE_CXX_COMPILER=g++ \
        -DCMAKE_BUILD_TYPE=Release \
        -DCMAKE_INSTALL_PREFIX=/stage-1/install \
        -S /llvm-project/clang \
        -B /stage-1/build/clang && \
    cmake \
        --build /stage-1/build/clang \
        --target all && \
    cmake \
        --build /stage-1/build/clang \
        --target install && \
    cmake \
        -GNinja \
        -DCMAKE_C_COMPILER=/stage-1/install/bin/clang \
        -DCMAKE_CXX_COMPILER=/stage-1/install/bin/clang++ \
        -DCMAKE_BUILD_TYPE=Release \
        -DCMAKE_INSTALL_PREFIX=/stage-2/install \
        -DCOMPILER_RT_BUILD_BUILTINS=YES \
        -DCOMPILER_RT_BUILD_SANITIZERS=NO \
        -DCOMPILER_RT_BUILD_XRAY=NO \
        -DCOMPILER_RT_BUILD_LIBFUZZER=YES \
        -DCOMPILER_RT_BUILD_PROFILE=YES \
        -DCOMPILER_RT_INCLUDE_TESTS=NO \
        -S /llvm-project/compiler-rt \
        -B /stage-2/build/compiler-rt && \
    cmake \
        --build /stage-2/build/compiler-rt \
        --target all && \
    cmake \
        --build /stage-2/build/compiler-rt \
        --target install && \
    mkdir -p /stage-1/install/lib/clang/${CLANG_VERSION}/lib/linux/ && \
    cp /stage-2/install/lib/linux/*.a \
        /stage-1/install/lib/clang/${CLANG_VERSION}/lib/linux/ && \
    mkdir -p /stage-3/install/lib/clang/${CLANG_VERSION}/lib/linux/ && \
    cp /stage-2/install/lib/linux/*.a \
        /stage-3/install/lib/clang/${CLANG_VERSION}/lib/linux/ && \
    cmake \
        -GNinja \
        -DCMAKE_C_COMPILER=gcc \
        -DCMAKE_CXX_COMPILER=g++ \
        -DCMAKE_BUILD_TYPE=Release \
        -DCMAKE_INSTALL_PREFIX=/stage-1/install \
        -DCLANG_DEFAULT_RTLIB=compiler-rt \
        -S /llvm-project/clang \
        -B /stage-1/build/clang && \
    cmake \
        --build /stage-1/build/clang \
        --target all && \
    cmake \
        --build /stage-1/build/clang \
        --target install && \
    cmake \
        -GNinja \
        -DCMAKE_C_COMPILER=/stage-1/install/bin/clang \
        -DCMAKE_CXX_COMPILER=/stage-1/install/bin/clang++ \
        -DCMAKE_BUILD_TYPE=Release \
        -DCMAKE_INSTALL_PREFIX=/stage-1/install \
        -DBUILD_SHARED_LIBS=YES \
        -S /llvm-project/lld \
        -B /stage-1/build/lld && \
    cmake \
        --build /stage-1/build/lld \
        --target all && \
    cmake \
        --build /stage-1/build/lld \
        --target install && \
    ln -s /stage-1/install/bin/ld.lld /stage-1/install/bin/ld && \
    cmake \
        -GNinja \
        -DCMAKE_C_COMPILER=gcc \
        -DCMAKE_CXX_COMPILER=g++ \
        -DCMAKE_BUILD_TYPE=Release \
        -DCMAKE_INSTALL_PREFIX=/stage-1/install \
        -DCLANG_DEFAULT_RTLIB=compiler-rt \
        -DCLANG_DEFAULT_LINKER=lld \
        -S /llvm-project/clang \
        -B /stage-1/build/clang && \
    cmake \
        --build /stage-1/build/clang \
        --target all && \
    cmake \
        --build /stage-1/build/clang \
        --target install && \
    cmake \
        -GNinja \
        -DCMAKE_C_COMPILER=/stage-1/install/bin/clang \
        -DCMAKE_CXX_COMPILER=/stage-1/install/bin/clang++ \
        -DCMAKE_BUILD_TYPE=Release \
        -DCMAKE_INSTALL_PREFIX=/stage-2/install \
        -DBUILD_SHARED_LIBS=YES \
        -DLIBUNWIND_ENABLE_SHARED=YES \
        -DLIBUNWIND_ENABLE_STATIC=NO \
        -DLIBUNWIND_INSTALL_SHARED_LIBRARY=YES \
        -DLIBUNWIND_INSTALL_STATIC_LIBRARY=NO \
        -DLIBUNWIND_USE_COMPILER_RT=YES \
        -DLIBUNWIND_TARGET_TRIPLE=$(uname -m)-alpine-linux-musl \
        -S /llvm-project/libunwind \
        -B /stage-2/build/libunwind && \
    cmake \
        --build /stage-2/build/libunwind \
        --target all && \
    cmake \
        --build /stage-2/build/libunwind \
        --target install && \
    cmake \
        -GNinja \
        -DCMAKE_C_COMPILER=gcc \
        -DCMAKE_CXX_COMPILER=g++ \
        -DCMAKE_BUILD_TYPE=Release \
        -DCMAKE_INSTALL_PREFIX=/stage-1/install \
        -DCLANG_DEFAULT_RTLIB=compiler-rt \
        -DCLANG_DEFAULT_LINKER=lld \
        -DCLANG_DEFAULT_UNWINDLIB=libunwind \
        -S /llvm-project/clang \
        -B /stage-1/build/clang && \
    cmake \
        --build /stage-1/build/clang \
        --target all && \
    cmake \
        --build /stage-1/build/clang \
        --target install && \
    export LIBRARY_PATH=/stage-2/install/lib && \
    cmake \
        -GNinja \
        -DCMAKE_C_COMPILER=/stage-1/install/bin/clang \
        -DCMAKE_CXX_COMPILER=/stage-1/install/bin/clang++ \
        -DCMAKE_BUILD_TYPE=Release \
        -DCMAKE_INSTALL_PREFIX=/stage-2/install \
        -DBUILD_SHARED_LIBS=YES \
        -DLIBCXX_ENABLE_RTTI=YES \
        -DLIBCXX_ENABLE_SHARED=YES \
        -DLIBCXX_ENABLE_STATIC=NO \
        -DLIBCXX_ENABLE_EXPERIMENTAL_LIBRARY=NO \
        -DLIBCXX_INCLUDE_DOCS=NO \
        -DLIBCXX_INCLUDE_TESTS=NO \
        -DLIBCXX_INCLUDE_BENCHMARKS=NO \
        -DLIBCXX_INSTALL_SHARED_LIBRARY=YES \
        -DLIBCXX_INSTALL_STATIC_LIBRARY=NO \
        -DLIBCXX_USE_COMPILER_RT=YES \
        -DLIBCXX_HAS_MUSL_LIBC=YES \
        -DLIBCXX_HAS_ATOMIC_LIB=NO \
        -DLIBCXX_HAVE_CXX_ATOMICS_WITH_LIB=NO \
        -DLIBCXXABI_USE_LLVM_UNWINDER=YES \
        -DLIBCXX_TARGET_TRIPLE=$(uname -m)-alpine-linux-musl \
        -S /llvm-project/libcxx \
        -B /stage-2/build/libcxx && \
    cmake \
        --build /stage-2/build/libcxx \
        --target all && \
    cmake \
        --build /stage-2/build/libcxx \
        --target install && \
    export CPATH=/llvm-project/libunwind/include && \
    cmake \
        -GNinja \
        -DCMAKE_C_COMPILER=/stage-1/install/bin/clang \
        -DCMAKE_CXX_COMPILER=/stage-1/install/bin/clang++ \
        -DCMAKE_BUILD_TYPE=Release \
        -DCMAKE_INSTALL_PREFIX=/stage-2/install \
        -DBUILD_SHARED_LIBS=YES \
        -DLIBCXXABI_ENABLE_SHARED=YES \
        -DLIBCXXABI_ENABLE_STATIC=NO \
        -DLIBCXXABI_INCLUDE_TESTS=NO \
        -DLIBCXXABI_INSTALL_SHARED_LIBRARY=YES \
        -DLIBCXXABI_INSTALL_STATIC_LIBRARY=NO \
        -DLIBCXXABI_LIBUNWIND_INCLUDES=/llvm-project/libunwind/include \
        -DLIBCXXABI_LIBUNWIND_PATH=/llvm-project/libunwind \
        -DLIBCXXABI_LIBCXX_LIBRARY_PATH=/stage-2/install/lib \
        -DLIBCXXABI_LIBCXX_PATH=/llvm-project/libcxx \
        -DLIBCXXABI_USE_COMPILER_RT=YES \
        -DLIBCXXABI_USE_LLVM_UNWINDER=YES \
        -DLIBCXXABI_TARGET_TRIPLE=$(uname -m)-alpine-linux-musl \
        -S /llvm-project/libcxxabi \
        -B /stage-2/build/libcxxabi && \
    cmake \
        --build /stage-2/build/libcxxabi \
        --target all && \
    cmake \
        --build /stage-2/build/libcxxabi \
        --target install && \
    cmake \
        -GNinja \
        -DCMAKE_C_COMPILER=/stage-1/install/bin/clang \
        -DCMAKE_CXX_COMPILER=/stage-1/install/bin/clang++ \
        -DCMAKE_BUILD_TYPE=Release \
        -DCMAKE_INSTALL_PREFIX=/stage-2/install \
        -DBUILD_SHARED_LIBS=YES \
        -DLIBCXX_ENABLE_RTTI=YES \
        -DLIBCXX_ENABLE_SHARED=YES \
        -DLIBCXX_ENABLE_STATIC=NO \
        -DLIBCXX_ENABLE_EXPERIMENTAL_LIBRARY=NO \
        -DLIBCXX_INCLUDE_DOCS=NO \
        -DLIBCXX_INCLUDE_TESTS=NO \
        -DLIBCXX_INCLUDE_BENCHMARKS=NO \
        -DLIBCXX_INSTALL_SHARED_LIBRARY=YES \
        -DLIBCXX_INSTALL_STATIC_LIBRARY=NO \
        -DLIBCXX_USE_COMPILER_RT=YES \
        -DLIBCXX_HAS_MUSL_LIBC=YES \
        -DLIBCXX_HAS_ATOMIC_LIB=NO \
        -DLIBCXX_HAVE_CXX_ATOMICS_WITH_LIB=NO \
        -DLIBCXXABI_USE_LLVM_UNWINDER=YES \
        -DLIBCXX_TARGET_TRIPLE=$(uname -m)-alpine-linux-musl \
        -DLIBCXX_CXX_ABI=libcxxabi \
        -DLIBCXX_CXX_ABI_INCLUDE_PATHS=/llvm-project/libcxxabi/include \
        -DLIBCXX_CXX_ABI_LIBRARY_PATH=/stage-2/install/lib \
        -S /llvm-project/libcxx \
        -B /stage-2/build/libcxx && \
    cmake \
        --build /stage-2/build/libcxx \
        --target all && \
    cmake \
        --build /stage-2/build/libcxx \
        --target install && \
    cmake \
        -GNinja \
        -DCMAKE_C_COMPILER=gcc \
        -DCMAKE_CXX_COMPILER=g++ \
        -DCMAKE_BUILD_TYPE=Release \
        -DCMAKE_INSTALL_PREFIX=/stage-1/install \
        -DCLANG_DEFAULT_RTLIB=compiler-rt \
        -DCLANG_DEFAULT_LINKER=lld \
        -DCLANG_DEFAULT_CXX_STDLIB=libc++ \
        -DCLANG_DEFAULT_UNWINDLIB=libunwind \
        -S /llvm-project/clang \
        -B /stage-1/build/clang && \
    cmake \
        --build /stage-1/build/clang \
        --target all && \
    cmake \
        --build /stage-1/build/clang \
        --target install && \
    export CPATH=/stage-2/install/include/c++/v1/ && \
    export LD_LIBRARY_PATH=/stage-2/install/lib && \
    export LIBRARY_PATH=/stage-2/install/lib && \
    cmake \
        -GNinja \
        -DCMAKE_C_COMPILER=/stage-1/install/bin/clang \
        -DCMAKE_CXX_COMPILER=/stage-1/install/bin/clang++ \
        -DCMAKE_BUILD_TYPE=Release \
        -DCMAKE_INSTALL_PREFIX=/stage-3/install \
        -DCMAKE_EXE_LINKER_FLAGS=-lc++abi \
        -DCMAKE_SHARED_LINKER_FLAGS=-lc++abi \
        -DLLVM_BUILD_DOCS=NO \
        -DLLVM_BUILD_TESTS=NO \
        -DLLVM_BUILD_EXAMPLES=NO \
        -DLLVM_BUILD_BENCHMARKS=NO \
        -DLLVM_INCLUDE_DOCS=NO \
        -DLLVM_INCLUDE_TESTS=NO \
        -DLLVM_INCLUDE_EXAMPLES=NO \
        -DLLVM_INCLUDE_BENCHMARKS=NO \
        -DLLVM_ENABLE_LIBCXX=YES \
        -DLLVM_ENABLE_LLD=YES \
        -DLLVM_ENABLE_PIC=YES \
        -DLLVM_ENABLE_FFI=YES \
        -DLLVM_ENABLE_ZLIB=YES \
        -DLLVM_ENABLE_RTTI=YES \
        -DLLVM_ENABLE_SPHINX=NO \
        -DLLVM_ENABLE_DOXYGEN=NO \
        -DLLVM_ENABLE_OCAMLDOC=NO \
        -DLLVM_LINK_LLVM_DYLIB=YES \
        -DLLVM_BUILD_LLVM_DYLIB=YES \
        -DLLVM_INSTALL_BINUTILS_SYMLINKS=YES \
        -DLLVM_HOST_TRIPLE=$(uname -m)-alpine-linux-musl \
        -DLLVM_DEFAULT_TARGET_TRIPLE=$(uname -m)-alpine-linux-musl \
        -S /llvm-project/llvm \
        -B /stage-3/build/llvm && \
    cmake \
        --build /stage-3/build/llvm \
        --target all && \
    cmake \
        --build /stage-3/build/llvm \
        --target install && \
    export PATH=/stage-3/install/bin:$PATH && \
    export LIBRARY_PATH=/stage-3/install/lib:$LIBRARY_PATH && \
    cmake \
        -GNinja \
        -DCMAKE_C_COMPILER=/stage-1/install/bin/clang \
        -DCMAKE_CXX_COMPILER=/stage-1/install/bin/clang++ \
        -DCMAKE_BUILD_TYPE=Release \
        -DCMAKE_INSTALL_PREFIX=/stage-3/install \
        -DCMAKE_EXE_LINKER_FLAGS=-lc++abi \
        -DCMAKE_SHARED_LINKER_FLAGS=-lc++abi \
        -DBUILD_SHARED_LIBS=YES \
        -S /llvm-project/lld \
        -B /stage-3/build/lld && \
    cmake \
        --build /stage-3/build/lld \
        --target all && \
    cmake \
        --build /stage-3/build/lld \
        --target install && \
    cmake \
        -GNinja \
        -DCMAKE_C_COMPILER=/stage-1/install/bin/clang \
        -DCMAKE_CXX_COMPILER=/stage-1/install/bin/clang++ \
        -DCMAKE_BUILD_TYPE=Release \
        -DCMAKE_INSTALL_PREFIX=/stage-3/install \
        -DCMAKE_EXE_LINKER_FLAGS=-lc++abi \
        -DCMAKE_SHARED_LINKER_FLAGS=-lc++abi \
        -DCLANG_DEFAULT_RTLIB=compiler-rt \
        -DCLANG_DEFAULT_LINKER=lld \
        -DCLANG_DEFAULT_CXX_STDLIB=libc++ \
        -DCLANG_DEFAULT_UNWINDLIB=libunwind \
        -S /llvm-project/clang \
        -B /stage-3/build/clang; \
    cmake \
        --build /stage-3/build/clang \
        --target all || true; \
    cmake \
        --build /stage-3/build/clang \
        --target all && \
    cmake \
        --build /stage-3/build/clang \
        --target install && \
    mkdir -p /usr/local/lib && \
    cp -r /stage-3/install/* /usr/local/ && \
    cp -r /stage-2/install/* /usr/local/ && \
    cp /usr/lib/gcc/x86_64-alpine-linux-musl/*/*.a /usr/local/lib/ && \
    cp /usr/lib/gcc/x86_64-alpine-linux-musl/*/crt*.o /usr/local/lib/ && \
    cp /usr/lib/libgcc_s.so /usr/local/lib/ && \
    unlink /usr/local/lib/libc++.so && \
    echo "INPUT(libc++.so.1 -lc++abi -lunwind)" > /usr/local/lib/libc++.so && \
    chmod 755 /usr/local/lib/libc++.so

FROM alpine

ARG BUILD_DATE
ARG VCS_REF
ARG VERSION

# Build-time metadata as defined at http://label-schema.org
LABEL \
    maintainer="tymoteusz.blazejczyk.pl@gmail.com" \
    org.label-schema.schema-version="1.0" \
    org.label-schema.build-date=$BUILD_DATE \
    org.label-schema.name="tymonx/clang" \
    org.label-schema.description="A Docker image with the Clang C/C++ compiler" \
    org.label-schema.usage="https://gitlab.com/tymonx/docker-clang/README.md" \
    org.label-schema.url="https://gitlab.com/tymonx/docker-clang" \
    org.label-schema.vcs-url="https://gitlab.com/tymonx/docker-clang" \
    org.label-schema.vcs-ref=$VCS_REF \
    org.label-schema.vendor="tymonx" \
    org.label-schema.version=$VERSION \
    org.label-schema.docker.cmd=\
"docker run --rm --user $(id -u):$(id -g) --volume $(pwd):$(pwd) --workdir $(pwd) --entrypoint /bin/sh tymonx/clang"

COPY --from=builder /usr/local /usr/local/

ENV \
    CC=clang \
    CXX=clang++

RUN apk add --quiet --no-cache \
        musl-dev \
        libedit \
        libffi && \
    cd /usr/local/bin && \
    ln -s ld.lld ld && \
    ln -s clang cc && \
    ln -s clang++ c++ && \
    cd -
